from flask import Flask,render_template
app=Flask(__name__)
app.config['PROPAGATE_EXCEPTIONS'] = True

@app.route('/')
@app.route('/index')
def index():
 return render_template('index.html')
